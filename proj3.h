/**
 * IZP Project 03 / Single linkage clustering
 * @file proj3.h
 *
 * @author   Adrian Kiraly (xkiral01@stud.fit.vutbr.cz) [1BIA]
 * @date     2015-12-14
 * @version  1.1
 *
 * Simple single linkage clustering in 2D space. Further details of used method
 * are described at http://is.muni.cz/th/172767/fi_b/5739129/web/web/slsrov.html
 *
 *
 * @defgroup cops Cluster operations
 * @defgroup arops Cluster array operations
 */

/// Stores information about a single object
struct obj_t {
    /// Object ID
    int id;
    /// X-axis position
    float x;
    /// Y-axis position
    float y;
};

/// Holds objects contained in a cluster
struct cluster_t {
    /// Number of objects in cluster
    int size;
    /// Current capacity of underlying data structure
    int capacity;
    /// Array used as underlying data structure, containing objects
    struct obj_t *obj;
};

/**
 * @ingroup    cops
 * @brief      Initialize cluster
 *
 * Initializes a new cluster_t by allocating memory for `cap` objects (obj_t)
 * and setting size and capacity.
 *
 * If allocation failed, capacity is implicitly set to 0.
 *
 * @param      c     Pointer to cluster_t to initialize
 * @param[in]  cap   Requested capacity
 *
 * @pre      c != NULL
 * @pre      cap >= 0
 */
void init_cluster(struct cluster_t *c, int cap);

/**
 * @ingroup    cops
 * @brief      Clear cluster
 *
 * Clears cluster_t by freeing allocated memory and setting size and capacity
 * to 0.
 *
 * @param      c     Pointer to cluster_t to clear
 */
void clear_cluster(struct cluster_t *c);

/// Chunk of cluster objects. Value recommended for reallocation.
extern const int CLUSTER_CHUNK;

/**
 * @ingroup    cops
 * @brief      Resize cluster
 *
 * Reallocates memory for storing obj_t to a new size specified by new_cap and
 * sets clusters capacity to new value.
 *
 * @param      c        Pointer to cluster_t to resize
 * @param[in]  new_cap  New capacity of cluster
 *
 * @return     `c` on success, `NULL` if reallocation failed
 *
 * @pre      new_cap >= 0
 * @pre      c->capacity >= 0
 */
struct cluster_t *resize_cluster(struct cluster_t *c, int new_cap);

/**
 * @ingroup    cops
 * @brief      Append object to cluster
 *
 * Adds `obj` to the end of target cluster. If the capacity of target cluster
 * isn't high enough, calls resize_cluster() and allocates space for
 * CLUSTER_CHUNK more objects.
 *
 * @param      c     Pointer of target cluster_t
 * @param[in]  obj   Object to append
 */
void append_cluster(struct cluster_t *c, struct obj_t obj);

/**
 * @ingroup    cops
 * @brief      Merge clusters
 *
 * Merges clusters `c1` and `c2` by copying objects from `c2` to `c1` and then
 * sorts cluster `c1`. Cluster `c2` is left intact.
 *
 * @param      c1    First cluster
 * @param      c2    Second cluster
 *
 * @post     `c1` is sorted by object id in ascending order
 */
void merge_clusters(struct cluster_t *c1, struct cluster_t *c2);

/**
 * @ingroup    arops
 * @brief      Remove cluster from array
 *
 * Removes cluster with index `idx` from `carr`. Doesn't preserve order of
 * elements in array.
 *
 * @param      carr  Pointer to array of cluster_t
 * @param[in]  narr  Size of array
 * @param[in]  idx   Index of cluster to remove
 *
 * @return     New size of array
 *
 * @pre      idx < narr
 * @pre      narr > 0
 */
int remove_cluster(struct cluster_t *carr, int narr, int idx);

/**
 * @brief      Calculate euclidean distance of 2 objects
 *
 * @param      o1    First object
 * @param      o2    Second object
 *
 * @return     Distance of objects `o1` and `o2`
 */
float obj_distance(struct obj_t *o1, struct obj_t *o2);

/**
 * @ingroup    cops
 * @brief      Calculate distance of 2 clusters
 *
 * Calculates distance of clusters by finding closest pair of objects that don't
 * belong to the same cluster.
 *
 * @param      c1    First cluster
 * @param      c2    Second cluster
 *
 * @return     Distance of clusters `c1` and `c2`
 */
float cluster_distance(struct cluster_t *c1, struct cluster_t *c2);

/**
 * @ingroup    arops
 * @brief      Find closest clusters
 *
 * Finds two closest clusters in `carr` and saves their indexes to memory
 * pointed by `c1` and `c2`
 *
 * @param      carr  Pointer to array of cluster_t
 * @param[in]  narr  Size of array
 * @param[out] c1    Pointer for variable where index of first cluster will be
 *                   saved
 * @param[out] c2    Pointer for variable where index of second cluster will be
 *                   saved
 *
 * @pre      narr > 0
 */
void find_neighbours(struct cluster_t *carr, int narr, int *c1, int *c2);

/**
 * @ingroup    cops
 * @brief      Sort cluster objects
 *
 * Sorts objects in cluster by object id in ascending order.
 *
 * @param      c     Pointer to cluster_t to sort
 */
void sort_cluster(struct cluster_t *c);

/**
 * @ingroup    cops
 * @brief      Print cluster objects
 *
 * Prints space separated list of objects in `c` to `stdout` with a format
 * `id[x, y]`
 *
 * @param      c     Pointer to cluster_t to print
 */
void print_cluster(struct cluster_t *c);

/**
 * @ingroup    arops
 * @brief      Load objects from file
 *
 * Allocates memory for objects and saves address to pointer pointed by arr,
 * then loads objects from text files and saves each into a new cluster.
 *
 * In case of error, *arr is set to `NULL`, an error message is printed to
 * `stderr` and errno is set according to error type. If any clusters were
 * initialized, number of them will be returned in return value.
 *
 * @param      filename  Path to file
 * @param      arr       Pointer to pointer to array of cluster_t
 *
 * @return     Number of loaded objects
 */
int load_clusters(char *filename, struct cluster_t **arr);

/**
 * @ingroup    arops
 * @brief      Print clusters in array
 *
 * Prints all clusters and their objects from `carr`.
 *
 * @param      carr  Pointer to array of cluster_t
 * @param[in]  narr  Size of array
 */
void print_clusters(struct cluster_t *carr, int narr);
